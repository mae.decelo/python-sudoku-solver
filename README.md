# Python - Sudoku Solver

This is a simple implementation that demonstrates the use of breadth-first search to solve a Sudoku puzzle. 

## Implementation

The program is written in Python and be compiled using the command:

```
python sudoku_solver.py
```

It utilises a breadth-first search algorithm to traverse all cells within the sudoku
puzzle. 

---

## Usage

When the program is compiled, a list of information will be printed into the terminal. 

Firstly, the program will print a header containing information about the program. Empty 
cells are represented with 0s:

```
[9, 0, 0, 1, 7, 0, 4, 0, 2]
[1, 6, 0, 0, 4, 0, 0, 9, 5]
[0, 0, 8, 0, 0, 3, 0, 0, 0]
[0, 1, 0, 9, 0, 0, 5, 7, 3]
[0, 4, 0, 0, 0, 0, 0, 2, 0]
[5, 8, 9, 0, 0, 7, 0, 1, 0]
[0, 0, 0, 4, 0, 0, 7, 0, 0]
[6, 7, 0, 0, 2, 0, 0, 5, 8]
```

As the program performs a breadth-first search through the adjacency list of each node in the 
sudoku puzzle, it will determine and print the possible numbers that can be placed in the empty
cells according to the rules of sudoku:

```
----------------------------
NODE: A2
Possible numbers: [3, 5]
----------------------------
```

If the program finds an empty cell with only one valid option, it will update the value of that 
cell in the sudoku puzzle to that value. The program will print which node has been updated and 
to which value:

```
----------------------------
NODE D1 CHANGED TO 2
----------------------------
```

As empty cells are updated with values from 1 to 9, the nodes are added to a solved list. At the
end of the program, it will print the final state of the sudoku puzzle, as well as the length of 
the solved list:

```
End state of puzzle: 
[9, 3, 5, 1, 7, 6, 4, 8, 2]
[1, 6, 7, 8, 4, 2, 3, 9, 5]
[4, 2, 8, 5, 9, 3, 1, 6, 7]
[2, 1, 6, 9, 8, 4, 5, 7, 3]
[7, 4, 3, 6, 1, 5, 8, 2, 9]
[5, 8, 9, 2, 3, 7, 6, 1, 4]
[8, 5, 2, 4, 6, 9, 7, 3, 1]
[6, 7, 4, 3, 2, 1, 9, 5, 8]
[3, 9, 1, 7, 5, 8, 2, 4, 6]
SOLVED LENGTH: 79
```

The program ends if it reaches the goal node, and that goal node is not an empty cell. 

---

## Author

Anna Mae Decelo ([adecelo@myune.edu.au](mailto:adecelo@myune.edu.au), student ID 220226741)

