"""
    COSC550 Programming Assignment - Sudoku Puzzle
    Author: Anna Mae Decelo, student ID 220226741
"""

# START GLOBALS

# Adjacency list of each node
node_graph = {
    "A1": ["A2", "B1"],
    "A2": ["A1", "A3", "B2"],
    "A3": ["A2", "A4", "B3"],
    "A4": ["A3", "A5", "B4"],
    "A5": ["A4", "A6", "B5"],
    "A6": ["A5", "A7", "B6"],
    "A7": ["A6", "A8", "B7"],
    "A8": ["A7", "A9", "B8"],
    "A9": ["A8", "B9"],

    "B1": ["A1", "B2", "C1"],
    "B2": ["A2", "B1", "B3", "C2"],
    "B3": ["A3", "B2", "B4", "C3"],
    "B4": ["A4", "B3", "B5", "C4"],
    "B5": ["A5", "B4", "B6", "C5"],
    "B6": ["A6", "B5", "B7", "C6"],
    "B7": ["A7", "B6", "B8", "C7"],
    "B8": ["A8", "B7", "B9", "C8"],
    "B9": ["A9", "B8", "C9"],

    "C1": ["B1", "C2", "D1"],
    "C2": ["B2", "C1", "C3", "D2"],
    "C3": ["B3", "C2", "C4", "D3"],
    "C4": ["B4", "C3", "C5", "D4"],
    "C5": ["B5", "C4", "C6", "D5"],
    "C6": ["B6", "C5", "C7", "D6"],
    "C7": ["B7", "C6", "C8", "D7"],
    "C8": ["B8", "C7", "C9", "D8"],
    "C9": ["B9", "C8", "D9"],

    "D1": ["C1", "D2", "E1"],
    "D2": ["C2", "D1", "D3", "E2"],
    "D3": ["C3", "D2", "D4", "E3"],
    "D4": ["C4", "D3", "D5", "E4"],
    "D5": ["C5", "D4", "D6", "E5"],
    "D6": ["C6", "D5", "D7", "E6"],
    "D7": ["C7", "D6", "D8", "E7"],
    "D8": ["C8", "D7", "D9", "E8"],
    "D9": ["C9", "D8", "E9"],

    "E1": ["D1", "E2", "F1"],
    "E2": ["D2", "E1", "E3", "F2"],
    "E3": ["D3", "E2", "E4", "F3"],
    "E4": ["D4", "E3", "E5", "F4"],
    "E5": ["D5", "E4", "E6", "F5"],
    "E6": ["D6", "E5", "E7", "F6"],
    "E7": ["D7", "E6", "E8", "F7"],
    "E8": ["D8", "E7", "E9", "F8"],
    "E9": ["D9", "E8", "F9"],

    "F1": ["E1", "F2", "G1"],
    "F2": ["E2", "F1", "F3", "G2"],
    "F3": ["E3", "F2", "F4", "G3"],
    "F4": ["E4", "F3", "F5", "G4"],
    "F5": ["E5", "F4", "F6", "G5"],
    "F6": ["E6", "F5", "F7", "G6"],
    "F7": ["E7", "F6", "F8", "G7"],
    "F8": ["E8", "F7", "F9", "G8"],
    "F9": ["E9", "F8", "G9"],

    "G1": ["F1", "G2", "H1"],
    "G2": ["F2", "G1", "G3", "H2"],
    "G3": ["F3", "G2", "G4", "H3"],
    "G4": ["F4", "G3", "G5", "H4"],
    "G5": ["F5", "G4", "G6", "H5"],
    "G6": ["F6", "G5", "G7", "H6"],
    "G7": ["F7", "G6", "G8", "H7"],
    "G8": ["F8", "G7", "G9", "H8"],
    "G9": ["F9", "G8", "H9"],

    "H1": ["G1", "H2", "I1"],
    "H2": ["G2", "H1", "G3", "I2"],
    "H3": ["G3", "H2", "G4", "I3"],
    "H4": ["G4", "H3", "G5", "I4"],
    "H5": ["G5", "H4", "G6", "I5"],
    "H6": ["G6", "H5", "G7", "I6"],
    "H7": ["G7", "H6", "G8", "I7"],
    "H8": ["G8", "H7", "G9", "I8"],
    "H9": ["G9", "H8", "I9"],

    "I1": ["I2", "H1"],
    "I2": ["I1", "I3", "H2"],
    "I3": ["I2", "I4", "H3"],
    "I4": ["I3", "I5", "H4"],
    "I5": ["I4", "I6", "H5"],
    "I6": ["I5", "I7", "H6"],
    "I7": ["I6", "I8", "H7"],
    "I8": ["I7", "I9", "H8"],
    "I9": ["I8", "H9"],
}

# Map of nodes in the sudoku board
nodes_map = [["A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9"],
             ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9"],
             ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9"],

             ["D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9"],
             ["E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9"],
             ["F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9"],

             ["G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9"],
             ["H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9"],
             ["I1", "I2", "I3", "I4", "I5", "I6", "I7", "I8", "I9"]]

# Values of each cell in the initial state of the sudoku puzzle
sudoku_map = [[9, 0, 0, 1, 7, 0, 4, 0, 2],
              [1, 6, 0, 0, 4, 0, 0, 9, 5],
              [0, 0, 8, 0, 0, 3, 0, 0, 0],

              [0, 1, 0, 9, 0, 0, 5, 7, 3],
              [0, 4, 0, 0, 0, 0, 0, 2, 0],
              [5, 8, 9, 0, 0, 7, 0, 1, 0],

              [0, 0, 0, 4, 0, 0, 7, 0, 0],
              [6, 7, 0, 0, 2, 0, 0, 5, 8],
              [3, 0, 1, 0, 5, 8, 0, 0, 6]]

# List to keep track of nodes already visited/solved
solved = []


# END GLOBALS


# Returns the index of the inner list containing the node and the index of the node within the inner list
def get_index(node):
    for inner_list in nodes_map:
        if node in inner_list:
            return nodes_map.index(inner_list), inner_list.index(node)


# Get corresponding value of node in sudoku puzzle according to the node indices
def get_value(node):
    x, y = get_index(node)
    value = sudoku_map[x][y]
    return value


# Find which 3 x 3 grid the node belongs to and return the corresponding sudoku values of nodes in the grid
def get_grid_values(x, y):
    grid_values = []

    # Upper section
    if int(x) in range(3):
        # Upper left
        if int(y) in range(3):
            for i in range(3):
                for j in range(3):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

        # Upper centre
        elif int(y) in range(3, 6):
            for i in range(3):
                for j in range(3, 6):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

        # Upper right
        elif int(y) in range(6, 9):
            for i in range(3):
                for j in range(6, 9):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

    # Middle section
    elif int(x) in range(3, 6):
        # Middle left
        if int(y) in range(3):
            for i in range(3, 6):
                for j in range(3):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

        # Middle centre
        elif int(y) in range(3, 6):
            for i in range(3, 6):
                for j in range(3, 6):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

        # Middle right
        elif int(y) in range(6, 9):
            for i in range(3, 6):
                for j in range(6, 9):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

    # Bottom section
    elif int(x) in range(6, 9):
        # Bottom left
        if int(y) in range(3):
            for i in range(6, 9):
                for j in range(3):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

        # Bottom centre
        elif int(y) in range(3, 6):
            for i in range(6, 9):
                for j in range(3, 6):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

        # Bottom right
        elif int(y) in range(6, 9):
            for i in range(6, 9):
                for j in range(6, 9):
                    grid_value = get_value(nodes_map[i][j])
                    grid_values.append(grid_value)

    return grid_values


# Find the valid number for each node that is an empty cell in the sudoku puzzle
def solve_node(node):
    vertical_values = []  # The numbers in the column that the node is in
    horizontal_values = []  # The numbers in the row that the node is in
    missing_vertical_numbers = []  # Missing numbers in column from range 1 to 9
    missing_horizontal_numbers = []  # Missing numbers in row from range 1 to 9
    missing_grid_numbers = []  # Missing numbers in grid from range 1 to 9
    possible_numbers = []  # List to store possible valid numbers

    # Get the corresponding sudoku value of the node
    sudoku_value = get_value(node)

    # Find the possible numbers for the node if it is an empty cell in the sudoku board
    if sudoku_value == 0:
        # Get position of node in the nodes_map
        x, y = get_index(node)

        # Get all values in column containing node
        for each_list in nodes_map:
            value = get_value(each_list[y])
            vertical_values.append(value)

        # Get all values in row containing node
        for each_node in nodes_map[x]:
            value = get_value(each_node)
            horizontal_values.append(value)

        # Get all values within grid containing node
        grid_values = get_grid_values(x, y)

        # Get missing numbers in each list - column, row and grid lists
        for number in range(1, 10):
            if number not in vertical_values:
                missing_vertical_numbers.append(number)

            if number not in horizontal_values:
                missing_horizontal_numbers.append(number)

            if number not in grid_values:
                missing_grid_numbers.append(number)

            # Add number to valid options if number is missing from column, row and grid
            if number in missing_vertical_numbers and number in missing_horizontal_numbers \
                    and number in missing_grid_numbers:
                possible_numbers.append(number)

        # Print the possible numbers for a given node
        print("----------------------------")
        print("NODE: " + node)
        print("Possible numbers: " + str(possible_numbers))
        print("----------------------------")

        # If there is only one valid option, set the sudoku value of the node to the valid option
        if len(possible_numbers) == 1:
            sudoku_map[x][y] = possible_numbers[0]

            # Print the node and sudoku board if a sudoku value is updated
            print("----------------------------")
            print("NODE " + node + " CHANGED TO " + str(possible_numbers[0]))
            print("----------------------------")


# Breadth-first search through all nodes in the node map
def bfs(graph, start, goal):
    global solved
    queue = [start]

    while queue:
        parent = queue.pop(0)

        # Solve the parent node if it is an empty cell in the sudoku puzzle
        if get_value(parent) == 0:
            solve_node(parent)

        # Find all children nodes of each parent nodes
        for child in graph[parent]:

            # Break from the while loop if the terminal node is reached and not an empty cell
            if child == goal and get_value(goal) > 0:
                queue = []
                break

            # Solve the node if not yet visited/solved
            if child not in solved:
                solve_node(child)
                queue.append(child)
                # print("QUEUED NODES: " + str(queue))

                # If node was solved, append to already visited/solved list
                if get_value(child) > 0:
                    solved.append(child)


def print_header():
    border       = "****************************************************************************"
    program      = "* Program Name   : COSC550 Sudoku Puzzle "
    description  = "* Description    : This program solves a 9x9 sudoku puzzle. It utilises a "
    description2 = "*                  breadth-first search to traverse all nodes in the puzzle."
    author       = "* Author         : Anna Mae Decelo (student ID 220226741)"
    contact      = "* Contact        : adecelo@myune.edu.au"

    print("\n" + border)
    print(program)
    print(description)
    print(description2)
    print(author)
    print(contact)
    print(border + "\n")


def main():
    global solved

    # Print header with information about author and concise description of the program
    print_header()

    # Print the initial state of the sudoku puzzle
    print("Initial state of puzzle:")
    for each_list in sudoku_map:
        print(each_list)

    # Run the bfs on the node map, starting at the first node, and ending at the last node
    bfs(node_graph, "A1", "I9")

    # Print the solution to the sudoku puzzle
    print("End state of puzzle: ")
    for each_list in sudoku_map:
        print(each_list)

    # Track the number of nodes added to the solved list
    print("SOLVED LENGTH: " + str(len(solved)))

    # End of the program
    print("*------- END OF PROGRAM ------*\n")


if __name__ == "__main__":
    main()
